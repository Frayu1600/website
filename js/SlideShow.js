﻿var index = 0;
var cur;

function toggleResponsive() {
    $(".container").toggleClass("responsive");
};

function carousel() {
    var y = $(".slide");
    var z = $(".indicator");

    y.each(function (i) {
        if (i == index) {
            $(this).children().show();
            $(this).show();
        } else {
            $(this).children().hide();
            $(this).hide();
        }
    });
    z.each(function (i) {
        if (i == index) $(this).css("background-color", "grey");
        else $(this).css("background-color", "lightgrey");
    });
    index++;
    if (index == y.length) index = 0;
    cur = setTimeout(carousel, 3200);
}


$(document).ready(function () {

    $(".droptoggle").click(toggleResponsive);

    $(".slideshow-container").each(function () {
        var indicators = $("<div class='indicators'></div>");
        indicators.appendTo(this);
        $(this).children(".slides").children(".slide").each(function (i) {
            var x = $("<span class='indicator'/>");
            x.click(function () {
                if (cur) {
                    clearTimeout(cur);
                    cur = undefined;
                }
                index = i;
                carousel();
            });
            x.appendTo(indicators);
        });
    });

    carousel();

    var windowWidth = $(window).width();
    if (windowWidth > 950) {
        $('.dropdown').hover(
            function () {
                $('.dropdown-content', this).stop(true, true).slideDown(400);
            },
            function () {
                $('.dropdown-content', this).stop(true, true).slideUp(400);
            });
    }
});
