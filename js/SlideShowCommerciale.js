﻿var lidl;
var sofim;

function Carousel(cls, interval) {
    // Copy 'this' to variable, so carousel can use it.
    var self = this;
    this.index = 0;
    this.elements = $(cls);
    this.startAt = function (x) {
        this.index = x;
        this.start();
    }
    this.start = function () {
        if (this.handle) clearInterval(this.handle);
        carousel();
        this.handle = setInterval(carousel, interval);

        function carousel() {
            self.elements.each(function (i) {
                if (i == self.index) $(this).show();
                else $(this).hide();
            });
            self.index++;
            if (self.index == self.elements.length) self.index = 0;
        }
    }
    this.stop = function () {
        if (!this.handle) return;
        clearInterval(this.handle);
        this.handle = undefined;
    }
}

$(document).ready(function () {
    lidl = new Carousel(".slideshow-container-lidl > .mySlides", 3200);
    sofim = new Carousel(".slideshow-container-sofim > .mySlides", 3200);

    $(".slideshow-buttons > button").click(function () {
        $(".slideshow-empty").hide();
    });

    $(".lidl").click(function () {
        $(".slideshow-container-sofim").hide();
        $(".slideshow-container-lidl").show();
        $(".info").html("Stai Visualizzando: LIDL Roncadelle");
        sofim.stop();
        lidl.startAt(0);
    });

    $(".sofim").click(function () {
        $(".slideshow-container-sofim").show();
        $(".slideshow-container-lidl").hide();
        $(".info").html("Stai Visualizzando: Sofim");
        lidl.stop();
        sofim.startAt(0);
    });
});