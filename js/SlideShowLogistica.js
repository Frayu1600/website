﻿var conad;
var benako;
var tribiano;
var crai;


function Carousel(cls, interval) {
    // Copy 'this' to variable, so carousel can use it.
    var self = this;
    this.index = 0;
    this.elements = $(cls);
    this.startAt = function (x) {
        this.index = x;
        this.start();
    }
    this.start = function () {
        if (this.handle) clearInterval(this.handle);
        carousel();
        this.handle = setInterval(carousel, interval);

        function carousel() {
            self.elements.each(function (i) {
                if (i == self.index) $(this).show();
                else $(this).hide();
            });
            self.index++;
            if (self.index == self.elements.length) self.index = 0;
        }
    }
    this.stop = function () {
        if (!this.handle) return;
        clearInterval(this.handle);
        this.handle = undefined;
    }
}

$(document).ready(function () {
    conad = new Carousel(".slideshow-container-conad > .mySlides", 3200);
    benako = new Carousel(".slideshow-container-benako > .mySlides", 3200);
    tribiano = new Carousel(".slideshow-container-tribiano > .mySlides", 3200);
    crai = new Carousel(".slideshow-container-crai > .mySlides", 3200)

    $(".slideshow-buttons > button").click(function () {
        $(".slideshow-empty").hide();
    });

    $(".benako").click(function () {
        $(".slideshow-container-conad").hide();
        $(".slideshow-container-tribiano").hide();
        $(".slideshow-container-crai").hide();
        $(".slideshow-container-benako").show();
        $(".info").html("Stai Visualizzando: Ortofin - Benako");
        conad.stop();
        tribiano.stop();
        crai.stop();
        benako.startAt(0);
    });

    $(".conad").click(function () {
        $(".slideshow-container-conad").show();
        $(".slideshow-container-benako").hide();
        $(".slideshow-container-crai").hide();
        $(".slideshow-container-tribiano").hide();
        $(".info").html("Stai Visualizzando: Conad");
        benako.stop();
        tribiano.stop();
        crai.stop();
        conad.startAt(0);
    });

    $(".tribiano").click(function () {
        $(".slideshow-container-tribiano").show();
        $(".slideshow-container-benako").hide();
        $(".slideshow-container-conad").hide();
        $(".slideshow-container-crai").hide();
        $(".info").html("Stai Visualizzando: Tribiano Ceva");
        benako.stop();
        crai.stop();
        conad.stop();
        tribiano.startAt(0);
    });

    $(".crai").click(function () {
        $(".slideshow-container-tribiano").hide();
        $(".slideshow-container-benako").hide();
        $(".slideshow-container-conad").hide();
        $(".slideshow-container-crai").show();
        $(".info").html("Stai Visualizzando: Crai");
        benako.stop();
        tribiano.stop();
        conad.stop();
        crai.startAt(0);
    });
});