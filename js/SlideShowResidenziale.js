﻿var babini;
var gregorini;
var portoferraio;
var ricci;
var iezza;
//var containers = jQuery.makeArray($(".slideshow-buttons").children());

function Carousel(cls, interval) {
    // Copy 'this' to variable, so carousel can use it.
    var self = this;
    this.index = 0;
    this.elements = $(cls);
    this.startAt = function (x) {
        this.index = x;
        this.start();
    }
    this.start = function () {
        if (this.handle) clearInterval(this.handle);
        carousel();
        this.handle = setInterval(carousel, interval);

        function carousel() {
            self.elements.each(function (i) {
                if (i == self.index) $(this).show();
                else $(this).hide();
            });
            self.index++;
            if (self.index == self.elements.length) self.index = 0;
        }
    }
    this.stop = function () {
        if (!this.handle) return;
        clearInterval(this.handle);
        this.handle = undefined;
    }
}

$(document).ready(function () {
    babini = new Carousel(".slideshow-container-babini > .mySlides", 3200);
    gregorini = new Carousel(".slideshow-container-gregorini > .mySlides", 3200);
    portoferraio = new Carousel(".slideshow-container-portoferraio > .mySlides", 3200);
    ricci = new Carousel(".slideshow-container-ricci > .mySlides", 3200);
    iezza = new Carousel(".slideshow-container-iezza > .mySlides", 3200);
    //containers.each(function (i) {
    //    this = new Carousel(".slideshow-container-" + this + "> .mySlides", 3200);
    //});

    $(".slideshow-buttons > button").click(function () {
        $(".slideshow-empty").hide();
    });

    $(".gregorini").click(function () {
        $(".slideshow-container-babini").hide();
        $(".slideshow-container-portoferraio").hide();
        $(".slideshow-container-ricci").hide();
        $(".slideshow-container-iezza").hide();
        $(".slideshow-container-gregorini").show();
        $(".info").html("Stai Visualizzando: SH - Gregorini");
        babini.stop();
        portoferraio.stop();
        ricci.stop();
        iezza.stop();
        gregorini.startAt(0);
    });

    $(".babini").click(function () {
        $(".slideshow-container-babini").show();
        $(".slideshow-container-portoferraio").hide();
        $(".slideshow-container-ricci").hide();
        $(".slideshow-container-iezza").hide();
        $(".slideshow-container-gregorini").hide();
        $(".info").html("Stai Visualizzando: SH - Babini");
        gregorini.stop();
        portoferraio.stop();
        ricci.stop();
        iezza.stop();
        babini.startAt(0);
    });

    $(".portoferraio").click(function () {
        $(".slideshow-container-babini").hide();
        $(".slideshow-container-portoferraio").show();
        $(".slideshow-container-ricci").hide();
        $(".slideshow-container-iezza").hide();
        $(".slideshow-container-gregorini").hide();
        $(".info").html("Stai Visualizzando: SH - Portoferraio");
        gregorini.stop();
        babini.stop();
        ricci.stop();
        iezza.stop();
        portoferraio.startAt(0);
    });

    $(".ricci").click(function () {
        $(".slideshow-container-babini").hide();
        $(".slideshow-container-portoferraio").hide();
        $(".slideshow-container-ricci").show();
        $(".slideshow-container-iezza").hide();
        $(".slideshow-container-gregorini").hide();
        $(".info").html("Stai Visualizzando: SH - Ricci");
        gregorini.stop();
        babini.stop();
        portoferraio.stop();
        iezza.stop();
        ricci.startAt(0);
    });

    $(".iezza").click(function () {
        $(".slideshow-container-babini").hide();
        $(".slideshow-container-portoferraio").hide();
        $(".slideshow-container-ricci").hide();
        $(".slideshow-container-iezza").show();
        $(".slideshow-container-gregorini").hide();
        $(".info").html("Stai Visualizzando: SH - Iezza");
        gregorini.stop();
        babini.stop();
        portoferraio.stop();
        ricci.stop();
        iezza.startAt(0);
    });
});